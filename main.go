package demoPkg

import "fmt"

func Hello(str string) string {
	return fmt.Sprintf("Hello %s!", str)
}
